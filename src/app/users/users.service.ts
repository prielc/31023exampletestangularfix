import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { Headers } from '@angular/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { environment } from './../../environments/environment';

@Injectable()
export class UsersService {

  getUsers(){
    //get users from the SLIM rest API (Don't say DB)
    return  this.http.get(environment.url + 'users');
  }

  getUsersFire(){
    return this.db.list('/users').valueChanges();    
  }

  postUser(data){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('name',data.name).append('phone',data.phone);
    return this.http.post(environment.url + 'users',params.toString(),options); 
  }

  deleteMessage(key){
    return this.http.delete(environment.url + 'users/'+key);
  }

  getUser(id) {
    return this.http.get(environment.url + 'users/'+id);
  }

  putUser(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('name',data.name).append('phone',data.phone);
    return this.http.put(environment.url + 'users/'+ key,params.toString(), options);
  }


  constructor(private http:Http, private db:AngularFireDatabase) { 
    
  }

}


